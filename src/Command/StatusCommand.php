<?php
declare(strict_types=1);

namespace Soong\Console\Command;

use Soong\Contracts\Extractor\ExtendedTaskStatusProvider;
use Soong\Contracts\Task\EtlTask;
use Soong\Task\SimpleTaskContainer;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Implementation of the console "status" command.
 */
class StatusCommand extends EtlCommand
{

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName("soong:status")
          ->setDescription("Report status of Soong tasks")
          ->setDefinition([
            $this->tasksArgument(false),
            $this->directoryOption(),
            new InputOption(
                'extended',
                null,
                InputOption::VALUE_NONE,
                'Extended status with more details'
            )
          ])
          ->setHelp(<<<EOT
The <info>status</info> provides info on available Soong tasks
EOT
          );
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var string[] $directoryNames */
        $directoryNames = $input->getOption('directory');
        $container = SimpleTaskContainer::instance(true);
        $this->registerCoreDependencies($container, $input, $output);
        $taskContainer = $this->loadConfiguration($directoryNames);
        $table = $this->initialiseOutputTable($input, $output);
        if (empty($taskList = $input->getArgument('tasks'))) {
            $taskList = array_keys($taskContainer->getAll());
        }
        foreach ($taskList as $id) {
            if ($task = $taskContainer->get($id)) {
                if ($task instanceof EtlTask) {
                    $taskStatus = $this->prepareDefaultStatus($id, $task);
                    if ($input->getOption('extended')) {
                        $this->addExtendedStatusDetails($task, $taskStatus);
                    }
                } else {
                    $taskStatus = [$id, 'N/A', 'N/A', 'N/A'];
                }
                $table->addRow($taskStatus);
            } else {
                $output->writeln("<error>$id not found</error>");
            }
        }
        $table->render();
        return 0;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return Table
     */
    private function initialiseOutputTable(InputInterface $input, OutputInterface $output): Table
    {
        $table = new Table($output);
        $headers = ['Task', 'Total', 'Processed', 'Unprocessed'];
        if ($input->getOption('extended')) {
            $headers[] = 'Details';
        }
        $table->setHeaders($headers);
        return $table;
    }

    /**
     * @param EtlTask $task
     * @param array $taskStatus
     * @return void
     */
    private function addExtendedStatusDetails(EtlTask $task, array &$taskStatus)
    {
        $extractor = $task->getExtractor();
        $keyMap = $task->getKeyMap();
        if (!$extractor instanceof ExtendedTaskStatusProvider || !$keyMap instanceof \Soong\Contracts\KeyMap\ExtendedTaskStatusProvider) {
            $taskStatus[] = 'Not supported';
            return;
        }
        $sourceKeysValues = $extractor->getExtractedKeysValues();
        $keyMapKeysValues = $keyMap->getExtractedKeysValues();
        $unprocessedByKey = $this->getUnprocessedKeyValues($sourceKeysValues, $keyMapKeysValues);
                if (is_null($unprocessedByKey)) {
                    $taskStatus[] = 'N/A';
                    return;
                }
                $resultsHeader = ["Unprocessed IDs:"];
                $results = [];
                foreach ($unprocessedByKey as $key => $diff) {
                    if (count($diff)) {
                        $results[] = "$key: " . implode(',', $diff);
                    }
                }
                if (!$results) {
                    $taskStatus[] = 'Partially supported';
            return;
                }
         $taskStatus[] = implode("\n", array_merge($resultsHeader, $results));
    }

    /**
     * @param string $taskId
     * @param EtlTask $task
     * @return array
     */
    private function prepareDefaultStatus(string $taskId, EtlTask $task): array
    {
        $total = $processed = $unprocessed = 'N/A';
        $extractor = $task->getExtractor();
        if ($extractor instanceof \Countable) {
            $total = $extractor->count();
        }
        $keyMap = $task->getKeyMap();
        if ($keyMap instanceof \Countable) {
            $processed = $keyMap->count();
        }
        if (is_int($total) && is_int($processed)) {
            $unprocessed = $total - $processed;
        }
        return [$taskId, $total, $processed, $unprocessed];
    }

    private function getUnprocessedKeyValues(array $sourceKeys, array $migratedSourceKeys): ?array
    {
        if (!$sourceKeys && !$migratedSourceKeys) {
            return null;
        }
        $keys = $sourceKeys ? array_keys($sourceKeys[0]) : ($migratedSourceKeys ? array_keys($migratedSourceKeys[0]) : null);
        if (is_null($keys)) {
            return null;
        }
        $flatSourceKeys = $flatMigratedKeys = $diff = [];
        foreach ($keys as $key) {
            $flatSourceKeys[$key] = array_map(fn($item) => $item[$key], $sourceKeys);
            $flatMigratedKeys[$key] = array_map(fn($item) => $item[$key], $migratedSourceKeys);
            $diff[$key] = array_values(array_diff($flatSourceKeys[$key], $flatMigratedKeys[$key]));
        }
        return $diff;
    }
}
