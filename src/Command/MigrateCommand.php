<?php

declare(strict_types=1);

namespace Soong\Console\Command;

use League\Pipeline\Pipeline;
use Psr\Log\LogLevel;
use Soong\Contracts\Exception\ComponentNotFound;
use Soong\Contracts\Task\EtlTask;
use Soong\Contracts\Task\TaskContainer;
use Soong\Logger\Logger;
use Soong\Task\MigrateBatchesOperation;
use Soong\Task\MigrateOperation;
use Soong\Task\SimpleTaskContainer;
use Soong\Task\EtlTaskOperation;
use Soong\Task\TaskPayload;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Implementation of the "migrate" console command.
 */
class MigrateCommand extends EtlCommand
{
    use Logger {
        log as protected doLog;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName("soong:migrate")
            ->setDescription("Migrate data from one place to another")
            ->setDefinition([
                $this->tasksArgument(),
                $this->directoryOption(),
                $this->selectOption(),
                $this->offsetOption(),
                $this->limitOption(),
                $this->batchSizeOption(),
                $this->batchOption(),
            ])
            ->setHelp(<<<EOT
The <info>migrate</info> command executes a Soong migration task
EOT
          );
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var string[] $directoryNames */
        $directoryNames = $input->getOption('directory');
        $tasks = $input->getArgument('tasks');
        $options = [
            'select' => $input->getOption('select'),
            'offset' => $input->getOption('offset'),
            'limit' => $input->getOption('limit'),
            'batch_size' => $input->getOption('batch_size'),
            'batch' => $input->getOption('batch')
        ];
        $container = SimpleTaskContainer::instance(true);
        $this->registerCoreDependencies($container, $input, $output);
        $taskContainer = $this->loadConfiguration($directoryNames, $options);
        if (!$this->validateTasks($container, $tasks, $output)) {
            return 1;
        }
        $pipeline = new Pipeline();
        foreach ($tasks as $id) {
            $task = $taskContainer->get($id);
            $migrateOperation = $this->prepareMigrateOperation($task, $options, $output);
            $pipeline = $pipeline->pipe($migrateOperation);
        }
        $payload = new TaskPayload($options);
        try {
            $pipeline->process($payload);
        } catch (\Exception|\Error $e) {
            $previousExceptionMessage = $e->getPrevious()?->getMessage();
            $this->log(LogLevel::CRITICAL, $e->getMessage() . ($previousExceptionMessage ? " Previous exception: $previousExceptionMessage" : ""));
            return 1;
        }
        return 0;
    }

    /**
     * @param EtlTask $task
     * @return EtlTaskOperation
     */
    private function prepareMigrateOperation(EtlTask $task, array $options, OutputInterface $output): EtlTaskOperation
    {
        if ($options['batch_size']) {
            $this->log(LogLevel::NOTICE, "<info>Added task '{$task->getId()}' to the queue for batch processing (batch size: {$options['batch_size']})</info>");
            return new MigrateBatchesOperation($task);
        }
        $this->log(LogLevel::NOTICE, "<info>Added task '{$task->getId()}' to the queue</info>");
        return new MigrateOperation($task);
    }

    /**
     * @param TaskContainer $taskContainer
     * @param array $taskIds
     * @param OutputInterface $output
     * @return bool
     */
    private function validateTasks(TaskContainer $taskContainer, array $taskIds, OutputInterface $output): bool
    {
        foreach ($taskIds as $id) {
            try {
                $taskContainer->get($id);
            } catch (ComponentNotFound) {
                $this->log(LogLevel::ERROR, "<error>Task '$id' not found</error>");
                return false;
            }
        }
        return true;
    }

    /**
     * @param $level
     * @param $message
     * @param array $context
     * @return void
     */
    private function log($level, $message, array $context = []): void
    {
        $this->doLog($level, $message, $context, ['memory_usage' => true, 'timestamp' => true, 'etl_prefix' => '[MC]']);
    }
}
