<?php

declare(strict_types=1);

namespace Soong\Console\Command;

use Psr\Log\LoggerInterface;
use Soong\Console\SoongDependencyProvider;
use Soong\Contracts\Task\DependenciesContainer;
use Soong\Task\SimpleTask;
use Symfony\Component\Cache\Psr16Cache;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Noodlehaus\Config;
use Soong\Contracts\Extractor\Extractor;
use Soong\Contracts\KeyMap\KeyMap;
use Soong\Contracts\Loader\Loader;
use Soong\Contracts\Task\TaskContainer;
use Soong\Data\BasicRecordFactory;
use Soong\Extractor\CountableExtractorBase;
use Soong\Filter\Select;
use Soong\Task\SimpleTaskContainer;
use Soong\Transformer\Property\Copy;
use Soong\Transformer\Record\PropertyMapper;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Base class for all Soong console commands.
 */
class EtlCommand extends Command
{

    /**
     * Configure the "tasks" command argument for one or more values.
     *
     * @param bool $required
     *   TRUE if an explicit task is required, FALSE otherwise.
     *
     * @return The configured command argument.
     */
    protected function tasksArgument(bool $required = true): InputArgument
    {
        $required = $required ? InputArgument::REQUIRED : InputArgument::OPTIONAL;
        return new InputArgument(
            'tasks',
            InputArgument::IS_ARRAY | $required,
            'List of task IDs to process'
        );
    }

    /**
     * Configure the "directory" option to be required with one or more values.
     *
     * @return The configured command option.
     */
    protected function directoryOption(): InputOption
    {
        return new InputOption(
            'directory',
            null,
            InputOption::VALUE_IS_ARRAY | InputOption::VALUE_REQUIRED,
            'List of directories containing tasks to migrate',
            ['config']
        );
    }

    /**
     * Configure the "select" option.
     *
     * @return The configured command option.
     */
    protected function selectOption(): InputOption
    {
        return new InputOption(
            'select',
            null,
            InputOption::VALUE_IS_ARRAY | InputOption::VALUE_OPTIONAL,
            'List of property name=value criteria'
        );
    }

    /**
     * Configure the "offset" option.
     *
     * @return The configured command option.
     */
    protected function offsetOption(): InputOption
    {
        return new InputOption(
            'offset',
            null,
            InputOption::VALUE_OPTIONAL,
            'Index of the first record the processing starts from. Acceptable values: >= 1'
        );
    }

    /**
     * Configure the "limit" option.
     *
     * @return The configured command option.
     */
    protected function limitOption(): InputOption
    {
        return new InputOption(
            'limit',
            null,
            InputOption::VALUE_OPTIONAL,
            'Maximum number of records to process'
        );
    }

    /**
     * Configure the "batch_size" option.
     *
     * @return The configured command option.
     */
    protected function batchSizeOption(): InputOption
    {
        return new InputOption(
            'batch_size',
            null,
            InputOption::VALUE_OPTIONAL,
            'Split the extracted records in batches of the specified size for every task. If provided "limit" option is ignored. Acceptable values: >= 1'
        );
    }

    /**
     * Configure the "batch" option.
     *
     * @return The configured command option.
     */
    protected function batchOption(): InputOption
    {
        return new InputOption(
            'batch',
            null,
            InputOption::VALUE_OPTIONAL,
            'Batch number if the current command handles a batch.'
        );
    }

    /**
     * Obtain all task configuration contained in the specified directories.
     *
     * @param string[] $directoryNames
     *   List of directories containing task configuration.
     * @param array $options
     *   List of command-line options.
     */
    protected function loadConfiguration(
        array $directoryNames,
        array $options = [],
    ) : TaskContainer {
        $container = SimpleTaskContainer::instance();
        $recordFactory = new BasicRecordFactory();
        foreach ($directoryNames as $directoryName) {
            $conf = Config::load($directoryName);
            $this->substituteConfigurationVars($conf);
            foreach ($conf->all() as $taskId => $configuration) {
                if (is_null($configuration)) {
                    continue;
                }
                $taskClass = $configuration['class'];
                $taskConfiguration = $configuration['configuration'];
                $this->substituteEnvironmentalVars($taskConfiguration);
                $taskConfiguration['container'] = $container;
                $taskConfiguration['record_factory'] = $recordFactory;
                $taskConfiguration['extract']['configuration']['record_factory'] =
                    $recordFactory;
                $taskConfiguration['extract'] = $this->getExtractor(
                    $taskConfiguration['extract'],
                    $options,
                    $taskId
                );
                if (isset($taskConfiguration['transform'])) {
                    $taskConfiguration['transform'] =
                        $this->getTransform($taskConfiguration['transform'], $container);
                }
                $taskConfiguration['load'] = $this->getLoader($taskConfiguration['load']);
                if (isset($taskConfiguration['key_map'])) {
                    $taskConfiguration['key_map'] = $this->getKeyMap(
                        $taskConfiguration['key_map'],
                        $taskConfiguration['extract']->getKeyProperties(),
                        $taskConfiguration['load']->getKeyProperties()
                    );
                }
                $taskFQId = SimpleTask::formatFQId(basename(dirname($directoryName)), $taskId);
                $container->add($taskId, new $taskClass($taskConfiguration, $taskFQId));
            }
        }
        return $container;
    }

    /**
     * Construct an Extractor instance.
     *
     * @param array $configuration
     *   The extractor's configuration.
     * @param array $options
     *   Runtime options which may affect the configuration.
     * @param string $id
     *   Id of the containing task.
     */
    protected function getExtractor(
        array $configuration,
        array $options,
        string $id
    ): ?Extractor {
        /** @var \Soong\Contracts\Extractor\Extractor $extractorClass */
        $extractorClass = $configuration['class'];
        $extractorConfiguration = $configuration['configuration'];
        if (!empty($options['select'])) {
            // Each expression arrives in the form "$name$op$value" = we need to
            // turn that into an array [$name, $op, $value].
            $criteria = [];
            // Note that if '=' is before '==' in the operator array, 'a==b'
            // will be parsed as 'a', '=', '=b'. To prevent this, make sure the
            // operators are sorted longest first.
            $operatorList = Select::OPERATORS;
            usort($operatorList, fn($a, $b) => $b <=> $a);
            $operatorExpression = implode('|', $operatorList);
            foreach ($options['select'] as $expression) {
                if (!preg_match(
                    "/(.*?)($operatorExpression)(.*)/",
                    (string) $expression,
                    $matches
                )) {
                    throw new \InvalidArgumentException("--select: Invalid expression $expression");
                }
                $criteria[] = [$matches[1], $matches[2], $matches[3]];
            }
            $extractorConfiguration['filters'][] = [
                'class' => Select::class,
                'configuration' => [
                    'criteria' => $criteria,
                ],
            ];
        }
        // Replace filter configuration with actual instances.
        if (!empty($extractorConfiguration['filters'])) {
            foreach ($extractorConfiguration['filters'] as $key => $filter) {
                $extractorConfiguration['filters'][$key] =
                    new $filter['class']($filter['configuration']);
            }
        }
        if (is_a($extractorClass, CountableExtractorBase::class, true)) {
            if (!isset($extractorConfiguration['cache_count'])) {
                $extractorConfiguration['cache_count'] = true;
            }
            $extractorConfiguration['cache'] = SimpleTaskContainer::instance()->getCache();
            $extractorConfiguration['cache_key'] = $id . '_count';
        }
        $extractor = new $extractorClass($extractorConfiguration);
        return $extractor;
    }

    /**
     * Construct the transformation pipeline.
     *
     * @param array $configuration
     *   A list of RecordTransformer configurations.
     *   Container so transformers can find tasks.
     * @return \Soong\Contracts\Transformer\RecordTransformer[]
     */
    protected function getTransform(array $configuration, TaskContainer $container): array
    {
        /** @var \Soong\Contracts\Transformer\RecordTransformer[] $recordTransformers */
        $recordTransformers = [];
        foreach ($configuration as $recordTransformer) {
            $recordTransformerClass = $recordTransformer['class'];
            $recordTransformerConfiguration = $recordTransformer['configuration'];
            if (PropertyMapper::class === $recordTransformerClass) {
                $recordTransformerConfiguration['property_map'] = [];
                foreach ($recordTransformer['configuration']['property_map'] as $property => $transformerList) {
                    // Shortcut for directly mapping properties.
                    if (is_string($transformerList)) {
                        $sourceProperty = $transformerList;
                        $transformerList = [
                            [
                                'class' => Copy::class,
                                'source_property' => $sourceProperty,
                            ],
                        ];
                    }
                    foreach ($transformerList as $transformerStuff) {
                        $transformerConfiguration = $transformerStuff['configuration'] ?? [];
                        // @todo Better way to provide context
                        $transformerConfiguration['container'] = $container;

                        /** @var \Soong\Contracts\Transformer\PropertyTransformer $transformerClass */
                        $transformerClass = $transformerStuff['class'];
                        $recordTransformerConfiguration['property_map'][$property][] = [
                            'transformer' => new $transformerClass($transformerConfiguration),
                            'source_property' => $transformerStuff['source_property'] ?? null,
                        ];
                    }
                }
            }
            $recordTransformers[] = new $recordTransformerClass($recordTransformerConfiguration);
        }

        return $recordTransformers;
    }

    /**
     * Construct the loader.
     *
     * @param array $configuration
     *   Configuration of the loader.
     */
    protected function getLoader(array $configuration): ?Loader
    {
        /** @var \Soong\Contracts\Loader\Loader $loaderClass */
        $loaderClass = $configuration['class'];
        return new $loaderClass($configuration['configuration']);
    }

    /**
     * Construct the key map.
     *
     * @param array $configuration
     *   Configuration of the key map.
     */
    protected function getKeyMap(
        array $configuration,
        array $extractorKeys,
        array $loaderKeys
    ): ?KeyMap {
        $keyMapConfiguration = $configuration['configuration'] ?? [];
        $keyMapConfiguration = array_merge(
            ['extractor_keys' => $extractorKeys],
            $keyMapConfiguration
        );
        $keyMapConfiguration = array_merge(
            ['loader_keys' => $loaderKeys],
            $keyMapConfiguration
        );
        /** @var \Soong\Contracts\KeyMap\KeyMap $keyMapClass */
        $keyMapClass = $configuration['class'];
        return new $keyMapClass($keyMapConfiguration);
    }

    /**
     * @todo: Add support for dependency configuration  as it's all hardcoded now.
     * @param DependenciesContainer $container
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function registerCoreDependencies(DependenciesContainer $container, InputInterface $input, OutputInterface $output)
    {
        (new SoongDependencyProvider($container))
            ->initialiseLogger($output)
            ->initialiseCache()
            ->initialiseStdin($input)
            ->initialiseStdout($output);
    }

    /**
     * Search for variable definitions and usages and replace variables with their actual values.
     *
     * @param Config $taskConfiguration
     */
    private function substituteConfigurationVars(Config $taskConfiguration): void
    {
        $variableCollections = [];
        foreach ($taskConfiguration->all() as $taskId => $value) {
            // If starts with a $ this is not a task ID but a variables collection.
            if (strpos($taskId, '$') === 0) {
                // Store variable collections separately and remove them from the original task configurations.
                $variableCollections = array_merge($variableCollections, $taskConfiguration->get($taskId));
                $taskConfiguration->remove($taskId);
            }
        }
        // Remove the empty (former variable collections) items from the configuration.
        $configurationData = array_filter($taskConfiguration->all());
        // Generate a lookup list of variable collection names as a regex for further usage.
        $variableNamesRegexp = $this->getVariableCollectionsRegexp($variableCollections);
        $this->substituteVariablesRecursive($variableCollections, $variableNamesRegexp, $configurationData);
        foreach ($configurationData as $dataKey => $dataValue) {
            $taskConfiguration->set($dataKey, $dataValue);
        }
    }

    /**
     * Recursively check if configuration values are variables and replaces them with the actual values.
     *
     * @param array  $variables
     * @param string $variableNamesRegexp
     * @param        $configuration
     */
    private function substituteVariablesRecursive(array $variables, string $variableNamesRegexp, &$configuration): void
    {
        if (is_array($configuration)) {
            foreach ($configuration as &$value) {
                $this->substituteVariablesRecursive($variables, $variableNamesRegexp, $value);
            }
        } elseif (is_string($configuration)) {
            // @todo: the below "quick checks" might be replaced by a more robust regex.
            // Escaped '$' reference is used (presumably in a regexp).
            if (strpos($configuration, '$$') !== false) {
                $variableNamesRegexp = str_replace('\$', '\$\$', $variableNamesRegexp);
            }
            // Single reference is used, add wrapping {} to avoid wrong matching in case of similar reference names.
            if (preg_match('/^\$[a-zA-Z]\w*$/', $configuration)) {
                $configuration = '{' . $configuration . '}';
            }
            $matchedReferences = [];
            if (preg_match_all("/$variableNamesRegexp/", $configuration, $matchedReferences)) {
                foreach ($matchedReferences[0] as $matchedReference) {
                    $matchedReferenceName = ltrim(trim($matchedReference, '{}'), '$');
                    if ($configuration === $matchedReference) {
                        $configuration = $variables[$matchedReferenceName];
                    } elseif (
                        is_string($variables[$matchedReferenceName]) || is_numeric($variables[$matchedReferenceName])
                    ) {
                        $configuration = str_replace(
                            $matchedReference,
                            (string) $variables[$matchedReferenceName],
                            $configuration
                        );
                    }
                }
            }
        }
    }

    /**
     * @param array|null $variableCollections
     * @return string
     */
    private function getVariableCollectionsRegexp(?array $variableCollections): string
    {
        $variableNamesRegexp = implode(
            '|',
            array_map(fn ($item) => '\{\$' . $item . '\}', array_keys($variableCollections))
        );
        return $variableNamesRegexp;
    }

    /**
     * @param mixed $configuration
     * @return void
     */
    private function substituteEnvironmentalVars(mixed &$configuration): void
    {
        $envMarker = '$env.';
        foreach ($configuration as &$value) {
            if (!is_array($value)) {
                if (is_string($value) && str_starts_with($value, $envMarker)) {
                    $envVarName = str_replace($envMarker, '', $value);
                    $value = env($envVarName);
                }
            } else {
                $this->substituteEnvironmentalVars($value);
            }
        }
    }

    /**
     * @param DependenciesContainer $container
     * @return void
     */
    private function registerCache(DependenciesContainer $container): void
    {
        $cacheDir = sys_get_temp_dir() . '/soong_cache';
        if (!is_dir($cacheDir)) {
            mkdir($cacheDir, 0777);
        }
        $cache = new Psr16Cache(new FilesystemAdapter('soong', 0, $cacheDir));
        $container->setCache($cache);
    }

    /**
     * @param OutputInterface $output
     * @param DependenciesContainer $container
     * @return void
     */
    private function registerLogger(OutputInterface $output, DependenciesContainer $container): void
    {
        $logger = interface_exists(LoggerInterface::class) ? new ConsoleLogger($output) : null;
        if ($logger) {
            $container->setLogger($logger);
        }
    }
}
