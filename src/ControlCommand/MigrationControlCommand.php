<?php

namespace Soong\Console\ControlCommand;

interface MigrationControlCommand
{
    public const STOP = 'stop';

    /**
     * Send a command to a running migration task.
     * @return bool
     */
    public function send(): bool;

    /**
     * Check if a command was previously sent.
     * @return bool
     */
    public function wasSent(): bool;

    /**
     * Mark the previously sent command as executed. Implementations must remove the command from command storage.
     * @return bool
     */
    public function markAsExecuted(): bool;
}
