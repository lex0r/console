<?php

namespace Soong\Console\ControlCommand;

use Symfony\Component\Cache\Psr16Cache;

class StopMigrationCommand implements MigrationControlCommand
{
    public function __construct(
        private string $taskFQid,
        private Psr16Cache $commandStorage
    ) {
    }

    /**
     * @inheritdoc
     */
    public function send(): bool
    {
        return $this->commandStorage->set($this->getTaskCommandKey(), MigrationControlCommand::STOP);
    }

    /**
     * @inheritdoc
     */
    public function wasSent(): bool
    {
        if ($this->commandStorage->has($this->getTaskCommandKey())) {
            return $this->commandStorage->get($this->getTaskCommandKey()) === MigrationControlCommand::STOP;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function markAsExecuted(): bool
    {
        return $this->commandStorage->delete($this->getTaskCommandKey());
    }

    /**
     * @return string
     */
    private function getTaskCommandKey(): string
    {
        return 'migration_control_command_' . $this->taskFQid;
    }
}
