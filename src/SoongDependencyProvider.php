<?php

namespace Soong\Console;

use Psr\Log\LoggerInterface;
use Soong\Task\SimpleTaskContainer;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Cache\Psr16Cache;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;

readonly class SoongDependencyProvider
{
    public function __construct(private SimpleTaskContainer $dependencyContainer)
    {
    }

    public function initialiseLogger(OutputInterface $output): self
    {
        $logger = interface_exists(LoggerInterface::class) ? new ConsoleLogger($output) : null;
        if ($logger) {
            $this->dependencyContainer->setLogger($logger);
        }
        return $this;
    }

    public function initialiseCache(): self
    {
        $cacheDir = sys_get_temp_dir() . '/soong_cache';
        if (!is_dir($cacheDir)) {
            mkdir($cacheDir, 0777);
        }
        $cache = new Psr16Cache(new FilesystemAdapter('soong', 0, $cacheDir));
        $this->dependencyContainer->setCache($cache);
        return $this;
    }

    public function initialiseStdin(InputInterface $input): self
    {
        $this->dependencyContainer->setStdin($input);
        return $this;
    }

    public function initialiseStdout(OutputInterface $output): self
    {
        $this->dependencyContainer->setStdout($output);
        return $this;
    }
}
