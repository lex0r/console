<?php

namespace Soong\Console\Tests\Transformer\Property;

use Soong\Console\Transformer\Property\UcFirst;
use Soong\Tests\Contracts\Transformer\PropertyTransformerTestBase;

/**
 * Tests the \Soong\Transformer\Property\UcFirst class.
 */
class UcFirstTest extends PropertyTransformerTestBase
{

    /**
     * Specify the class we're testing.
     */
    protected function setUp() : void
    {
        $this->transformerClass = '\\' . UcFirst::class;
    }

    /**
     * Test capitalization of various values.
     */
    public function transformerDataProvider() : array
    {
        return [
            'empty string' => [[], '', '', null],
            'null' => [[], null, null, null],
            'lowercase string' => [[], 'abc', 'Abc', null],
            'uppercase string' => [[], 'ABC', 'ABC', null],
            'non-alpha string' => [[], '12345', '12345', null],
            'integer' => [[], 12345, null,
                'UcFirst property transformer: expected string value, received integer'],
            'boolean' => [[], true, null,
                'UcFirst property transformer: expected string value, received boolean'],
            'array' => [[], [], null,
                'UcFirst property transformer: expected string value, received array'],
            'object' => [[], new \stdClass , null,
                'UcFirst property transformer: expected string value, received object'],
        ];
    }
}
