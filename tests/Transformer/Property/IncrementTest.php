<?php

namespace Soong\Console\Tests\Transformer\Property;

use Soong\Console\Transformer\Property\Increment;
use Soong\Tests\Contracts\Transformer\PropertyTransformerTestBase;

/**
 * Tests the \Soong\Transformer\Property\Increment class.
 */
class IncrementTest extends PropertyTransformerTestBase
{

    /**
     * Specify the class we're testing.
     */
    protected function setUp() : void
    {
        $this->transformerClass = '\\' . Increment::class;
    }

    /**
     * Test incrementing of various types of values
     */
    public function transformerDataProvider() : array
    {
        return [
            'positive integer' => [[], 1, 2, null],
            'negative integer' => [[], -1, 0, null],
            'numeric string' => [[], '1', 2, null],
            'zero' => [[], 0, 1, null],
            'null' => [[], null, null, null],
            'boolean' => [[], true, null,
                'Increment property transformer: expected numeric value, received boolean'],
            'non-numeric string' => [[], 'Blah', null,
                'Increment property transformer: expected numeric value, received string'],
            'array' => [[], ['Blah'], null,
                'Increment property transformer: expected numeric value, received array'],
            'object' => [[], new \stdClass, null,
                'Increment property transformer: expected numeric value, received object'],
        ];
    }
}
